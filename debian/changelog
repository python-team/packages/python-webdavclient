python-webdavclient (3.14.6-3) unstable; urgency=medium

  * Adopt package (Closes: #1087548, #1088058)
  * Remove dependency on python3-pkg-resources
  * Patch the one SyntaxWarning (Closes: #1086971)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 23 Nov 2024 16:16:14 +0100

python-webdavclient (3.14.6-2) unstable; urgency=medium

  * Team upload.
  * Remove extraneous dependency on python3-six
  * Set 'Rules-Requires-Root: no'
  * Use new dh-sequence-python3

 -- Alexandre Detiste <tchet@debian.org>  Wed, 24 Jan 2024 22:31:29 +0100

python-webdavclient (3.14.6-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 3.14.6.
  * Bump Standards-Version to 4.6.0.
  * debian/gbp.conf: Add configuration.

 -- Boyuan Yang <byang@debian.org>  Wed, 02 Feb 2022 23:03:22 -0500

python-webdavclient (3.14.5-2) unstable; urgency=low

  * Team upload.
  * Source-only upload for testing migration.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository.
  * Use canonical URL in Vcs-Git.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Boyuan Yang ]
  * debian/control: Bump debhelper compat to v13.
  * Bump Standards-Version to 4.5.1.

 -- Boyuan Yang <byang@debian.org>  Tue, 12 Jan 2021 15:26:34 -0500

python-webdavclient (3.14.5-1) unstable; urgency=medium

  * Initial release. (Closes: #961715)

 -- Johannes Tiefenbacher <jt@peek-a-boo.at>  Mon, 15 Jun 2020 22:29:02 +0200
